package io.dclarke.ecbfx.service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;

/**
 * @author Declan Clarke
 * @version 1.0
 */
public interface IFxService {

    Map<String, BigDecimal> getLatestRates(Currency base, List<Currency> to);

    Map<String, BigDecimal> getAllLatestRates(Currency base);

    BigDecimal convertValue(BigDecimal value, Currency base, Currency to);
}
