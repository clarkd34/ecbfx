package io.dclarke.ecbfx.service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.dclarke.ecbfx.repository.IFxRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author Declan Clarke
 * @version 1.0
 */
@Service
public class SimpleFxService implements IFxService {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleFxService.class);
    private static final String CACHE_NAME = "fxRates";

    private final IFxRepository fxRepo;
    private final CacheManager cacheManager;

    public SimpleFxService(CacheManager cacheManager, IFxRepository fxRepo) {
        this.cacheManager = cacheManager;
        this.fxRepo = fxRepo;
    }

    @Override
    public Map<String, BigDecimal> getLatestRates(Currency base, List<Currency> to) {
        Map<String, BigDecimal> baseRates = cacheManager.getCache(CACHE_NAME).get(base.getCurrencyCode(), Map.class);
        List<String> codes = to.stream().map(Currency::getCurrencyCode).collect(Collectors.toList());
        baseRates.keySet().removeIf(k -> !codes.contains(k));
        return baseRates;
    }

    @Override
    public Map<String, BigDecimal> getAllLatestRates(Currency base) {
        String key = base.getCurrencyCode();
        return cacheManager.getCache(CACHE_NAME).get(key, Map.class);
    }

    @Override
    public BigDecimal convertValue(BigDecimal value, Currency base, Currency to) {
        Map<String, BigDecimal> baseRates = cacheManager.getCache(CACHE_NAME).get(base.getCurrencyCode(), Map.class);
        return value.multiply(baseRates.get(to.getCurrencyCode()));
    }

    @PostConstruct
    @Scheduled(cron = "0 30 16 * * *")
    public void refreshRates() {
        LOG.info("Refreshing rates");
        Map<String, BigDecimal> euro = fxRepo.pullRates();
        cacheManager.getCache(CACHE_NAME).put("EUR", euro);

        // Calculate rates for each currency listed
        euro.forEach((cur, rate) -> {
            // Get the inverse rate of current currency against EUR
            BigDecimal inverse = BigDecimal.ONE.divide(rate, new MathContext(6))
                    .setScale(5, RoundingMode.HALF_EVEN);
            Map<String, BigDecimal> rates = new HashMap<>();

            // Add the rate as result for current currency
            rates.put("EUR", inverse);

            // Calculate rates for other currencies based on the inverse rate
            euro.forEach((c, r) -> rates.put(c, r.multiply(inverse, new MathContext(6))
                    .setScale(5, RoundingMode.HALF_EVEN)));

            // Remove duplicate currency value
            rates.remove(cur);

            // Cache rates
            cacheManager.getCache(CACHE_NAME).put(cur, rates);
        });
    }
}
