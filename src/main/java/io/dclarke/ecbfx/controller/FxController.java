package io.dclarke.ecbfx.controller;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;

import io.dclarke.ecbfx.service.IFxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Declan Clarke
 * @version 1.0
 */
@RestController
@RequestMapping("/fx")
public class FxController {
    private static final Logger LOG = LoggerFactory.getLogger(FxController.class);

    private final IFxService fxService;

    FxController(IFxService fxService) {
        this.fxService = fxService;
    }

    @GetMapping("/latest")
    public ResponseEntity<Map<String, BigDecimal>> getRates(@RequestParam(defaultValue = "EUR") Currency base,
                                                            @RequestParam List<Currency> to) {
        return ResponseEntity.ok(fxService.getLatestRates(base, to));
    }

    @GetMapping("/latest/all")
    public ResponseEntity<Map<String, BigDecimal>> getAllRates(@RequestParam(defaultValue = "EUR") Currency base) {
        return ResponseEntity.ok(fxService.getAllLatestRates(base));
    }

    @GetMapping("/convert")
    public ResponseEntity<BigDecimal> convertValue(@RequestParam BigDecimal value,
                                                   @RequestParam(defaultValue = "EUR") Currency base,
                                                   @RequestParam Currency to) {
        return ResponseEntity.ok(fxService.convertValue(value, base, to));
    }
}
