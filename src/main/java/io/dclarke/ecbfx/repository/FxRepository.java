package io.dclarke.ecbfx.repository;

import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipInputStream;

/**
 * @author Declan Clarke
 * @version 1.0
 */
@Repository
public class FxRepository implements IFxRepository {
    private static final Logger LOG = LoggerFactory.getLogger(FxRepository.class);
    private static final String RATES_URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref.zip";

    @Override
    public Map<String, BigDecimal> pullRates() {
        Map<String, BigDecimal> euroRates = new HashMap<>();
        RestTemplate template = new RestTemplate();

        byte[] bytes = template.getForObject(RATES_URL, byte[].class);

        byte[] csvBytes;
        try(ZipInputStream zipIn = new ZipInputStream(new ByteArrayInputStream(bytes))) {
            byte[] buffer = new byte[4096];
            int read, size = 0;

            zipIn.getNextEntry();
            do {
                read = zipIn.read(buffer);
                size = (read != -1) ? read : size;
            }
            while(read != -1);
            csvBytes = Arrays.copyOf(buffer, size);
        }
        catch(IOException e) {
            throw new RuntimeException(e);
        }

        try(InputStreamReader byteReader = new InputStreamReader(new ByteArrayInputStream(csvBytes))) {
            CSVReader csvReader = new CSVReader(byteReader, ',');
            String[] labels, rates;

            labels = csvReader.readNext();
            rates = csvReader.readNext();

            for(int i = 1; i < labels.length; i++) {
                if(labels[i].trim().isEmpty())
                    continue;

                Currency currency = Currency.getInstance(labels[i].trim());
                BigDecimal rate = new BigDecimal(rates[i].trim());

                euroRates.put(currency.getCurrencyCode(), rate);
            }
        }
        catch(IOException e) {
            throw new RuntimeException(e);
        }

        return euroRates;
    }
}
