package io.dclarke.ecbfx.repository;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Declan Clarke
 * @version 1.0
 */
public interface IFxRepository {

    Map<String, BigDecimal> pullRates();
}
