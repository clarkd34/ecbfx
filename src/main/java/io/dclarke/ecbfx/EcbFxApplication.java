package io.dclarke.ecbfx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcbFxApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcbFxApplication.class, args);
	}
}
